◇ BuildTools.jarの実行に使用する、Install.sh の内容は下記の通りです。

java -jar BuildTools.jar


◇ Spigotサーバーの起動に使用する、Start.bat の内容は下記の通りです。

@echo off
java -Xms1024M -Xmx1024M -jar spigot-1.8.7.jar
pause

上記の1024Mという部分を変更すれば、
サーバーに割り当てられるメモリの量を変更することができます。

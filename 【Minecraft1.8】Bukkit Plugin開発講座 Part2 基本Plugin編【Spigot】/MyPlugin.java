package com.example.myplugin;

import org.bukkit.plugin.java.JavaPlugin;

public class MyPlugin extends JavaPlugin
{
	@Override
	public void onEnable()
	{
		getLogger().info("プラグインが有効化されました！");
	}

	@Override
	public void onDisable()
	{
		getLogger().info("プラグインが無効化されました！");
	}
}
